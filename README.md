elsarticle-fixedbiblatex
========================

Article class for Elsevier journals, usuable with biblatex
----------------------------------------------------------
This version includes minor changes to make it work with biblatex. The multiple E-Mail issue is still *NOT* fixed.

The elsarticle manual still applies. The only difference is that you have to use `\auth{AUTHOR NAME}` instead of `\author{AUTHOR NAME}`.


Systems
-------
independent

Tested with TeX Live 2016 in Ubuntu 16.04.


Licence
-------
The article class `elsarticle-fixedbiblatex` is licenced under LPPL 1.2, just as the original class file from Elsevier.
